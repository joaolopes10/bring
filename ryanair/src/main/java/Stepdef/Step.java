package Stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step {				

	public static WebDriver driver=null;
	
    @Given("^I open the Ryanair website$")				
    public void open_the_Ryanair_website() throws Throwable							
    {	
    	// Atualização do sistema para o driver
    	System.setProperty("webdriver.chrome.driver","./src/Drivers/chromedriver.exe");
    	
    	//Cria do driver 
        driver = new ChromeDriver();
        
        // Maximar o broser 
        driver.manage().window().maximize();
        
        // definir a pagina a abrir 
       driver.get("https://www.ryanair.com/gb/en");					
    }		
    
    @And("^I Accept the Coockies$")					
    public void accept_the_cookies() throws Throwable 							
    {		
    	//Esperar que o elemento esteja visivel 
    	WebDriverWait wait = new WebDriverWait(driver, 10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"cookie-popup-with-overlay\"]//button[2]")));
    	
    	// clica no botoao 
    	driver.findElement(By.xpath("//*[@id=\"cookie-popup-with-overlay\"]//button[2]")).click();	
    }

    @And("^I Choose the Origine and Destination countrys$")					
    public void choose_the_origine_and_destination_country() throws Throwable 							
    {	
    	
    	driver.findElement(By.xpath("//*[@id=\"input-button__departure\"]")).sendKeys("Lisboa");	
    	driver.findElement(By.xpath("//*[@id=\"input-button__destination\"]")).click();
    	driver.findElement(By.xpath("//*[@id=\"input-button__destination\"]")).sendKeys("Paris");
    	
    	WebDriverWait wait = new WebDriverWait(driver, 10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Paris Beauvais')]")));
    	driver.findElement(By.xpath("//span[contains(text(),'Paris Beauvais')]")).click();
   	
    		
    }
    
    @And("^I Select the Depart and Return dates$")					
    public void select_the_depart_and_return() throws Throwable 							
    {	
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ry-tooltip-8\"]//fsw-datepicker")));
    	driver.findElement(By.xpath("(//*[@id='ry-tooltip-8']//fsw-datepicker//*[contains(text(),'Jun ')])[1]")).click();
   	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), ' June 2021')]")));
    	driver.findElement(By.xpath("//div[@data-id=\"2021-06-06\"]")).click();   	
    	driver.findElement(By.xpath("//month-toggle//*[@iconid=\"glyphs/chevron-right\"]")).click();
    	driver.findElement(By.xpath("(//*[contains(text(),\"Oct\")])[1]")).click();
    	driver.findElement(By.xpath("//div[@data-id=\"2021-10-30\"]")).click();
    	
    	
    }
   //click data picker para o numero de passageiros
    @And("^I Select the Passengers$")				
    public void select_the_passengers() throws Throwable 							
    {	
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),'Choose passengers based on their age at the time of travel.')]")));
    	//+adulto
    	driver.findElement(By.xpath("//*[@data-ref='passengers-picker__adults']//*[@data-ref='counter.counter__increment']")).click();
    	driver.findElement(By.xpath("//*[@data-ref=\"passengers-picker__children\"]//*[@data-ref='counter.counter__increment']")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Adults')]/../..//div[@data-ref='counter.counter__value'][text()='2']")));
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),\"Children\")]/../..//div[@data-ref=\"counter.counter__value\"][text()=\"1\"]")));
    	driver.findElement(By.xpath("//button[@aria-label=\"Done\"]")).click();
     	
    	
    }
 
    @And("^I Click on the Search button$")					
    public void click_on_the_search_button() throws Throwable 							
    {	
    	driver.findElement(By.xpath("//button[@aria-label='Search']")).click();
    	
    }
 
    @And("^I Change the departure and return dates$")					
    public void change_the_departure_and_return_dates() throws Throwable 							
    {	

    	
    }
    
    @And("^I Select the tarifa value$")					
    public void select_the_tarifa_value() throws Throwable 							
    {	

    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='card-header b2 ng-tns-c137-17']")));
    	driver.findElement(By.xpath("//div[@class='card-header b2 ng-tns-c137-17']")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[normalize-space(text())='Value']")));  	   	
    	driver.findElement(By.xpath("//h2[normalize-space(text())='Value']")).click();
    	driver.findElement(By.xpath("//div[@class='card-header b2 ng-tns-c137-14']")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[normalize-space(text())='Value']")));  	   	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='fare-card__button fare-card__price ry-button--outline-dark-blue']")));
    	driver.findElement(By.xpath("//button[@class='fare-card__button fare-card__price ry-button--outline-dark-blue']")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Log in later']")));
    	driver.findElement(By.xpath("//span[text()='Log in later']")).click();
    	    	
    }
    
    @And("^I Fill the passenger Information$")					
    public void fill_the_passenger_information() throws Throwable 							
    {	
    	//passeger 1
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//button[contains(@class,'dropdown')])[1]"))); 	
    	driver.findElement(By.xpath("(//button[contains(@class,'dropdown')])[1]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ry-dropdown-item[@data-ref='title-item-1']/button"))); 	
    	driver.findElement(By.xpath("//ry-dropdown-item[@data-ref='title-item-1']/button")).click();
    	driver.findElement(By.xpath("//input[@id='formState.passengers.ADT-0.name']")).sendKeys("Sonia");
    	driver.findElement(By.xpath("//input[@id='formState.passengers.ADT-0.surname']")).sendKeys("Pereira");
    	
    	//passeger 2
    	driver.findElement(By.xpath("(//button[contains(@class,'dropdown')])[2]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ry-dropdown-item[@data-ref='title-item-2']/button"))); 	
    	driver.findElement(By.xpath("//ry-dropdown-item[@data-ref='title-item-1']/button")).click();
    	driver.findElement(By.xpath("//input[@id='formState.passengers.ADT-1.name']")).sendKeys("Diogo");
    	driver.findElement(By.xpath("//input[@id='formState.passengers.ADT-1.surname']")).sendKeys("Bettencourt");
    	
    	//passeger 3
    	driver.findElement(By.xpath("//input[@id='formState.passengers.CHD-0.name']")).sendKeys("Ines");
    	driver.findElement(By.xpath("//input[@id='formState.passengers.CHD-0.surname']")).sendKeys("Marcal");
    	
    	//continue btn
    	driver.findElement(By.xpath("//button[contains(@class,'continue-flow__button')]")).click();
    	
    	
    }
    
    @And("^I Select The Seates$")					
    public void select_the_seates() throws Throwable 							
    {	
    	//close popup
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[normalize-space(text())='Flights']"))); 	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ry-modal-portal']//button"))); 	
    	driver.findElement(By.xpath("//*[@id='ry-modal-portal']//button")).click();
    	
    	//select seats
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@data-ref='seat-map__seat-row-6']//button)[1]"))); 	
    	driver.findElement(By.xpath("(//div[@data-ref='seat-map__seat-row-6']//button)[4]")).click();
    	driver.findElement(By.xpath("(//div[@data-ref='seat-map__seat-row-6']//button)[5]")).click();
    	driver.findElement(By.xpath("(//div[@data-ref='seat-map__seat-row-6']//button)[6]")).click();
    	
    	//btn next flight
    	driver.findElement(By.xpath("//button[normalize-space(text())='Next flight']")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='seats-prompt__container ng-star-inserted']//button[contains(text(),'Pick these seats')]")));
    	driver.findElement(By.xpath("//div[@class='seats-prompt__container ng-star-inserted']//button[contains(text(),'Pick these seats')]")).click();
    	
    	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2")));
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),' No, thanks ')]")));
    	driver.findElement(By.xpath("//button[contains(text(),' No, thanks ')]")).click();
    	
    	
    }
    
    @And("^I Select a small pacakge$")					
    public void select_a_small_pacakge() throws Throwable 							
    {	
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='ry-radio-circle-button__label'])[1]"))); 	
    	driver.findElement(By.xpath("(//*[@class='ry-radio-circle-button__label'])[1]")).click();
    	
    
    }
    
    @And("^I click continue until i See Overview page$")					
    public void click_continue_until_see_overview_page() throws Throwable 							
    {	
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Continue')]"))); 	
    	driver.findElement(By.xpath("//button[contains(text(),'Continue')]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Anything else for your trip?')]"))); 	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Continue')]"))); 
    	driver.findElement(By.xpath("//button[contains(text(),'Continue')]")).click();
    	  	
    	
    }
    
    @When("^I See the Overview page$")					
    public void see_the_overview_page() throws Throwable 							
    {	
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Plan your whole trip')]"))); 
    	
    }
    
    @Then("^My flilght should be added$")					
    public void my_flilght_should_be_added() throws Throwable 							
    {	
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Flights added to trip')]"))); 
    	
    	    	
    }
      
    @And("^Close browser$")					
    public void close_browser() throws Throwable 							
    {		
    	//fechar o browser
    	driver.close();
    }

}